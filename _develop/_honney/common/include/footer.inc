<div class="bFooter">
  <div class="bFooter_top">
    <div class="bFooter_top_l">
      <ul class="navFt">
        <li><a href="#">NEWS</a></li>
        <li><a href="#">MOVIES</a></li>
        <li><a href="#">GIRLS</a></li>
        <li><a href="#">GUIDESERVICE</a></li>
        <li><a href="#">SHOP</a></li>
        <li><a href="#">LOG</a></li>
        <li><a href="#">ABOUT</a></li>
      </ul>
      <!--/.nav-->
    </div>
    <!--/.left-->
    <div class="bFooter_top_r">
      <ul class="listSocial">
        <li><a href="#"><img src="/common/images/icon_fb.png" alt=""></a></li>
        <li><a href="#"><img src="/common/images/icon_tw.png" alt=""></a></li>
        <li><a href="#"><img src="/common/images/icon_int.png" alt=""></a></li>
        <li><a href="#"><img src="/common/images/icon_rss.png" alt=""></a></li>
      </ul>
      <!--/.nav-->
    </div>
    <!--/.right-->
  </div>
  <div class="bFooter_bottom">
    <p class="copyright">All right reserved. Honey Spot</p>
    <div class="pagetop" id="pagetop"><span><img src="/common/images/icon_up.png" alt=""></span></div>
  </div>
</div>
<!--/.bFooter-->