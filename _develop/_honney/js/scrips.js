window.addEventListener('DOMContentLoaded', function() {
  $('.sliderMain').slick({
    dots: true,
    infinite: true,
    speed: 800,
    arrows: false,
    autoplay: true,
    slidesToShow: 1,
    adaptiveHeight: true,
    fade: true
  });
});