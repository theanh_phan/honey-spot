$(document).ready(function() {
  $(".main-slider").slick({
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 6000,
    dots: false,
    arrows: true,
    responsive: [{
      breakpoint: 768,
      settings: {
        arrows: false,
        dots: false
      }
    }]
  });
  $('.info-main_slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    fade: true,
    autoplay: true,
    autoplaySpeed: 5000,
    asNavFor: '.slider-for_main'
  });
  $('.slider-for_main').slick({
    slidesToShow: 19,
    slidesToScroll: 1,
    asNavFor: '.info-main_slider',
    dots: false,
    autoplay:false,
    arrows: false,
    centerMode: true,
    focusOnSelect: true,
    responsive: [{
      breakpoint: 768,
      settings: {
        autoplay:true,
      }
    }]
  });
});